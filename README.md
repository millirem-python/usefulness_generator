# Implements a novel scoring algorithm and presentation for expert study of
# word lists, building on traditional scoring algorithms by additionally
# taking into account subwords that appear in main words and the frequencies of
# those subwords.  Creates a .csv file suitable for opening with Excel.
#
# TODO:  Comment code using standard Python doc tool.
# TODO:  Rewrite calc_alphagram_probs() for conciseness.
# TODO:  Find inflections of words and filter these out, per expert's request.
# TODO:  Make direct Excel output?
# TODO:  Reimplement as a web app to allow the user to change weights on the
#  fly?