# Word Usefulness Ranker
# Jim Miller, Portland, OR
# jimsemantic@gmail.com
# v1.0  5/8/2020


import re
import math
import time
import csv
# import openpyxl

import constants


class WordData:
    def __init__(self, word):
        self.word = word
        self.alphagram = None
        self.front_hooks = ''
        self.back_hooks = ''
        self.subwords = []
        self.un1 = None
        self.un1_rank = None

    def __str__(self):
        return ','.join([self.word, self.alphagram, self.front_hooks,
                         self.back_hooks, ' '.join(self.subwords), self.un1,
                         self.un1_rank])


class AnagramData:
    # List, by length of identical-letter-group, of dictionaries keyed by letter
    subprobs_memoized = []
    for _ in range(0, 12):
        subprobs_memoized.append({})

    def __init__(self, alphagram, word):
        self.alphagram = alphagram
        self.sorting_scrabetic_rep = ''
        self.scrabetic_rep = ''
        self.words = [word]
        self.letter_distrib = {}
        self.prob = None
        self.prob_rank = None
        self.highest_un1 = None
        self.highest_un1_rank = None

    def __str__(self):
        return ','.join([self.alphagram, ' '.join(self.words),
                         "%i" % self.highest_un1])

    def find_nth(self, haystack, needle, n):
        start = haystack.find(needle)
        while start >= 0 and n > 1:
            start = haystack.find(needle, start + len(needle))
            n -= 1
        return start

    def make_sorting_scrabetic_rep(self):
        for t_char in constants.SCRABETIC_ORDER_TEMPLATE:
            if t_char == 'x':
                self.sorting_scrabetic_rep += t_char
                continue
            for w_char in self.alphagram:
                if t_char == w_char:
                    self.sorting_scrabetic_rep += w_char

    def make_scrabetic_rep(self):
        blank_3_pos = self.find_nth(self.sorting_scrabetic_rep, 'x', 3)
        reduced_rep = self.sorting_scrabetic_rep[:blank_3_pos] + \
                      self.sorting_scrabetic_rep[(blank_3_pos + 1):]
        self.scrabetic_rep = re.sub('x+', ' ', reduced_rep.strip()).strip()

    # Memoization
    @staticmethod
    def get_lettergroup_prob(letter, length):
        if letter in AnagramData.subprobs_memoized[length - 1].keys():
            return AnagramData.subprobs_memoized[length - 1][letter]
        else:
            letter_dist = constants.TILE_DISTRIBUTION[letter]
            if letter_dist - length < 0 or length <= 0:
                prob = 0
            else:
                prob = math.factorial(letter_dist) / math.factorial(length) / \
                       math.factorial(letter_dist - length)
            AnagramData.subprobs_memoized[length - 1][letter] = prob
            return prob

    def calc_prob(self):
        for letter in self.alphagram:
            if letter not in self.letter_distrib.keys():
                self.letter_distrib[letter] = 1
            else:
                self.letter_distrib[letter] += 1
        alphagram_prob = 0
        for num_blanks in range(0, 3):
            if num_blanks == 0:
                with_num_blanks_prob = 1
                for letter in self.letter_distrib. \
                        keys():
                    with_num_blanks_prob *= AnagramData.get_lettergroup_prob(
                        letter, self.letter_distrib[letter])
                    if with_num_blanks_prob == 0:
                        break
                alphagram_prob += with_num_blanks_prob
            elif num_blanks == 1:
                for blank in self.letter_distrib.keys():
                    with_num_blanks_prob = 2
                    for letter in self.letter_distrib.keys():
                        letter_distrib = self.letter_distrib[letter]
                        if letter == blank:
                            remaining_of_letter = letter_distrib - 1
                            if remaining_of_letter <= 0:
                                continue
                            else:
                                with_num_blanks_prob *= \
                                    AnagramData.get_lettergroup_prob(
                                        letter, remaining_of_letter)
                        else:
                            with_num_blanks_prob *= AnagramData. \
                                get_lettergroup_prob(letter, letter_distrib)
                        if with_num_blanks_prob == 0:
                            break
                    alphagram_prob += with_num_blanks_prob
            elif num_blanks == 2:
                if len(self.alphagram) == 2:
                    alphagram_prob += 1
                    continue
                ind = {k: i for i, k in enumerate(self.letter_distrib.keys())}
                for blank1 in self.letter_distrib.keys():
                    for blank2 in self.letter_distrib.keys():
                        if ind[blank2] < ind[blank1]:
                            continue
                        with_num_blanks_prob = 1
                        if blank1 == blank2:
                            blank_letter_distrib = self.letter_distrib[blank2]
                            if blank_letter_distrib - 1 <= 0:
                                continue
                        for letter in self.letter_distrib.keys():
                            letter_distrib = self.letter_distrib[letter]
                            if letter == blank1 == blank2:
                                remaining_of_letter = letter_distrib - 2
                                if remaining_of_letter <= 0:
                                    continue
                                else:
                                    with_num_blanks_prob *= AnagramData. \
                                        get_lettergroup_prob(letter,
                                        remaining_of_letter)
                            elif letter == blank1 or letter == blank2:
                                remaining_of_letter = letter_distrib - 1
                                if remaining_of_letter <= 0:
                                    continue
                                else:
                                    with_num_blanks_prob *= AnagramData. \
                                        get_lettergroup_prob(letter,
                                        remaining_of_letter)
                            else:
                                with_num_blanks_prob *= AnagramData. \
                                    get_lettergroup_prob(letter, letter_distrib)
                            if with_num_blanks_prob == 0:
                                break
                        alphagram_prob += with_num_blanks_prob
        self.prob = alphagram_prob


def read_words_of_length(length, wordlist):
    with open(f'data/CSW19_{length}s.txt', 'rt') as f:
        for line in f:
            wordlist.append(line.strip())
    return wordlist


def find_hooks(longer_length_wordlist, shorter_length_dict):
    for word in longer_length_wordlist:
        if word[1:] in shorter_length_dict.keys():
            shorter_length_dict[word[1:]].front_hooks += word[0]
        if word[:-1] in shorter_length_dict.keys():
            shorter_length_dict[word[:-1]].back_hooks += word[-1]


def aggregate_words_by_anagram(wordlist, word_anagram_dict, word_dict):
    for word in wordlist:
        alphagram = ''.join(sorted(word))
        word_dict[word].alphagram = alphagram
        if alphagram not in word_anagram_dict:
            word_anagram_dict[alphagram] = AnagramData(alphagram, word)
        else:
            word_anagram_dict[alphagram].words.append(word)


def find_subwords_of_length(subword_length, word_length, subword_dict,
                            word_dict):
    last_check_pos = word_length - subword_length
    for word in word_dict.keys():
        pos = 0
        while pos <= last_check_pos:
            subword_cand = word[pos: pos + subword_length]
            if subword_cand in subword_dict.keys():
                word_dict[word].subwords.append(subword_cand)
            pos += 1


def calc_UN1s(word_dict, word_anagram_dict, subword_dict, subword_anagram_dict):
    for word_data in word_dict.values():
        un1 = word_anagram_dict[word_data.alphagram].prob
        for subword in word_data.subwords:
            un1_sub = subword_anagram_dict[subword_dict[
                subword].alphagram].prob
            un1_sub /= constants.UN1_SUBWORD_SCALING_FACTOR
            un1_sub += 1
            un1_sub *= constants.UN1_SUBWORD_WEIGHT
            un1 *= un1_sub
        word_data.un1 = un1


def calc_highest_UN1s(word_dict, word_anagram_dict):
    for anagram_data in word_anagram_dict.values():
        un1s = [word_dict[word].un1 for word in anagram_data.words]
        anagram_data.highest_un1 = max(un1s)


def sort_and_rank_anagram_dict(word_anagram_dict):
    items = {k: v for k, v in sorted(word_anagram_dict.items(), reverse=True,
                                     key=lambda item: item[1].prob)}
    for rank, anagram_data in enumerate(items.values(), start=1):
        anagram_data.prob_rank = rank
    items = {k: v for k, v in sorted(word_anagram_dict.items(), reverse=True,
                                     key=lambda item: item[1].highest_un1)}
    for rank, anagram_data in enumerate(items.values(), start=1):
        anagram_data.highest_un1_rank = rank
    return items


def sort_and_rank_dict(word_dict):
    items = {k: v for k, v in sorted(word_dict.items(), reverse=True,
                                     key=lambda item: item[1].un1)}
    for rank, word_data in enumerate(items.values(), start=1):
        word_data.un1_rank = rank
    return items


def output_to_csv(word_dict, word_anagram_dict):
    with open('data/Usefulness_9s_1.csv', 'w', newline='') as f:
        csvw = csv.writer(f)
        csvw.writerow(["SCRABETIC REP", "SORTING SCRABETIC REP",
                       "HIGHEST UN1 RANK", "RAW PROB. RANK", "ANSWERS",
                       "ANSWERS UN1 RANKS", "ANSWERS SUBWORDS",
                       "ANSWERS # OF SUBWORDS"])
        for val in word_anagram_dict.values():
            words_data = [word_dict[word] for word in val.words]
            answer_with_hooks_groups = []
            un1_rank_groups = []
            subwords_groups = []
            number_of_subwords_groups = []
            for word_data in words_data:
                answer_with_hooks_groups.append(' '.join([word_data.front_hooks,
                                                word_data.word,
                                                word_data.back_hooks]).strip())
                un1_rank_groups.append(str(word_data.un1_rank))
                subwords_groups.append(' '.join(word_data.subwords).strip())
                number_of_subwords_groups.append(str(len(word_data.subwords)))
            csvw.writerow([val.scrabetic_rep,
                           val.sorting_scrabetic_rep,
                           val.highest_un1_rank,
                           val.prob_rank,
                           '\n'.join(answer_with_hooks_groups),
                           '\n'.join(un1_rank_groups),
                           '\n'.join(subwords_groups),
                           '\n'.join(number_of_subwords_groups)])


if __name__ == "__main__":
    start_time = time.time()
    start_process_time = time.process_time()

    list_2s = read_words_of_length(2, [])
    list_9s = read_words_of_length(9, [])
    list_10s = read_words_of_length(10, [])

    dict_2s = {}
    for word in list_2s:
        dict_2s[word] = WordData(word)

    dict_9s = {}
    for word in list_9s:
        dict_9s[word] = WordData(word)

    find_hooks(list_10s, dict_9s)

    dict_anagrams_9s = {}
    aggregate_words_by_anagram(list_9s, dict_anagrams_9s, dict_9s)
    dict_anagrams_2s = {}
    aggregate_words_by_anagram(list_2s, dict_anagrams_2s, dict_2s)

    find_subwords_of_length(2, 9, dict_2s, dict_9s)

    for alphagram in dict_anagrams_2s.values():
        alphagram.calc_prob()
    for alphagram in dict_anagrams_9s.values():
        alphagram.calc_prob()

    calc_UN1s(dict_9s, dict_anagrams_9s, dict_2s, dict_anagrams_2s)
    calc_highest_UN1s(dict_9s, dict_anagrams_9s)
    dict_anagrams_9s = sort_and_rank_anagram_dict(dict_anagrams_9s)
    dict_9s = sort_and_rank_dict(dict_9s)

    for val in dict_anagrams_9s.values():
        val.make_sorting_scrabetic_rep()
    for val in dict_anagrams_9s.values():
        val.make_scrabetic_rep()

    output_to_csv(dict_9s, dict_anagrams_9s)

    print("\n--- %s process seconds ---" % (time.process_time() -
                                        start_process_time))
    print("--- %s wall seconds ---" % (time.time() - start_time))
